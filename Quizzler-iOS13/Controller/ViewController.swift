//
//  ViewController.swift
//  Quizzler-iOS13
//
//  Created by Angela Yu on 12/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}

class ViewController: UIViewController {
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    var quizBrain = QuizBrain()
    
    @objc func updateUI() {
        let textValue = quizBrain.getText()
        let progressValue = quizBrain.getProgress()
        let scoreValue = quizBrain.scoreValue()
        scoreLabel.text = scoreValue
        UIView.animate(withDuration: 0.3, delay: 0.0, animations: {
            self.questionLabel.text = textValue
            self.questionLabel.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: 0.3) {
            self.progressBar.setProgress(progressValue, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    @IBAction func restartQuestions(_ sender: UIButton) {
        initialButtonAnimation()
        quizBrain.restart()
        updateUI()
    }
    
    func initialButtonAnimation() {
        UIView.animate(withDuration: 0.25, delay: 0.0, animations: {
            self.questionLabel.alpha = 0
        }, completion: nil)
    }
    
    @IBAction func answerButtonPressed(_ sender: UIButton) {
        initialButtonAnimation()
        let userAnswer = sender.currentTitle!
        let userIsCorrect = quizBrain.checkAnswer(userAnswer)
        if userIsCorrect {
            quizBrain.scoreModifier()
            UIView.animate(withDuration: 0.5, delay: 0.0, animations: {
                sender.backgroundColor = UIColor.green
                sender.backgroundColor = UIColor.clear
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, animations: {
                sender.backgroundColor = UIColor.red
                sender.backgroundColor = UIColor.clear
            }, completion: nil)
        }
        quizBrain.nextQuestion()
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateUI), userInfo: nil, repeats: false)
    }
}

